#include "emulator/machine-v2.h"

#include "misc/filebuffer.h"
#include "misc/fuselib.h"

namespace {
constexpr int kTicksPerFrame = 228 * 311;
}  // namespace

uint8_t MachineV2::run(uint8_t break_on_events, uint64_t max_ops) {
  for (uint64_t op = 0; op < max_ops; ++op) {
    events_ = 0;
    on_step();
    if (events_ & kEventInterrupt) on_handle_active_int();
    if (break_on_events & events_) return events_;
  }
  return 0;
}

void MachineV2::set_rzx_writer(std::unique_ptr<RzxWriter> rzx_writer) {
  rzx_writer_ = std::move(rzx_writer);
}

void MachineV2::load_snapshot(const std::string& filename) {
  FuseSnap snap;
  FileBuffer buffer(filename);
  CHECK_EQ(0, libspectrum_snap_read(
                  snap.snap(),
                  reinterpret_cast<const std::uint8_t*>(buffer.data().data()),
                  buffer.data().size(), LIBSPECTRUM_ID_UNKNOWN,
                  buffer.filename().c_str()));

  memory().load_from_snap(snap);

  set_pc(libspectrum_snap_pc(snap.snap()));
  set_a(libspectrum_snap_a(snap.snap()));
  set_f(libspectrum_snap_f(snap.snap()));
  set_sp(libspectrum_snap_sp(snap.snap()));
  set_hl(libspectrum_snap_hl(snap.snap()));
  set_de(libspectrum_snap_de(snap.snap()));
  set_bc(libspectrum_snap_bc(snap.snap()));
  set_is_halted(libspectrum_snap_halted(snap.snap()));
  set_ix(libspectrum_snap_ix(snap.snap()));
  set_iy(libspectrum_snap_iy(snap.snap()));
  set_i(libspectrum_snap_i(snap.snap()));
  set_r(libspectrum_snap_r(snap.snap()));
  set_alt_bc(libspectrum_snap_bc_(snap.snap()));
  set_alt_de(libspectrum_snap_de_(snap.snap()));
  set_alt_hl(libspectrum_snap_hl_(snap.snap()));
  set_alt_af(z80::make16(libspectrum_snap_a_(snap.snap()),
                         libspectrum_snap_f_(snap.snap())));
  set_iff1(libspectrum_snap_iff1(snap.snap()));
  set_iff2(libspectrum_snap_iff2(snap.snap()));
  set_int_mode(libspectrum_snap_im(snap.snap()));
  if (rzx_writer_) rzx_writer_->add_snapshot(std::move(snap));
}

void MachineV2::save_snapshot(const std::string& filename,
                              libspectrum_id_t format) {
  FuseSnap snap;
  memory().save_to_snap(&snap);

  libspectrum_snap_set_machine(snap.snap(), machine_model);
  libspectrum_snap_set_tstates(snap.snap(), frame_tick_);
  libspectrum_snap_set_pc(snap.snap(), get_pc());
  libspectrum_snap_set_a(snap.snap(), get_a());
  libspectrum_snap_set_f(snap.snap(), get_f());
  libspectrum_snap_set_sp(snap.snap(), get_sp());
  libspectrum_snap_set_hl(snap.snap(), get_hl());
  libspectrum_snap_set_de(snap.snap(), get_de());
  libspectrum_snap_set_bc(snap.snap(), get_bc());
  libspectrum_snap_set_halted(snap.snap(), is_halted());
  libspectrum_snap_set_ix(snap.snap(), get_ix());
  libspectrum_snap_set_iy(snap.snap(), get_iy());
  libspectrum_snap_set_i(snap.snap(), get_i());
  libspectrum_snap_set_r(snap.snap(), get_r());
  libspectrum_snap_set_bc_(snap.snap(), get_alt_bc());
  libspectrum_snap_set_de_(snap.snap(), get_alt_de());
  libspectrum_snap_set_hl_(snap.snap(), get_alt_hl());
  libspectrum_snap_set_iff1(snap.snap(), get_iff1());
  libspectrum_snap_set_iff2(snap.snap(), get_iff2());
  libspectrum_snap_set_im(snap.snap(), get_int_mode());
  libspectrum_snap_set_a_(snap.snap(), z80::get_high8(get_alt_af()));
  libspectrum_snap_set_f_(snap.snap(), z80::get_low8(get_alt_af()));

  unsigned char* buffer = nullptr;
  size_t length = 0;
  int flags;
  FuseCreator creator;
  CHECK_EQ(libspectrum_snap_write(&buffer, &length, &flags, snap.snap(), format,
                                  creator.creator(), 0),
           0);

  if (flags & LIBSPECTRUM_FLAG_SNAPSHOT_MAJOR_INFO_LOSS) {
    LOG(WARNING) << "A large amount of information has been lost in conversion;"
                    " the snapshot probably won't work";
  } else if (flags & LIBSPECTRUM_FLAG_SNAPSHOT_MINOR_INFO_LOSS) {
    LOG(WARNING) << "Some information has been lost in conversion;"
                    " the snapshot may not work";
  }

  write_string_to_file(
      filename, std::string_view(reinterpret_cast<char*>(buffer), length));
  libspectrum_free(buffer);
}

void MachineV2::save_cpu_state(V2CpuState* state) const {
  state->tstates = frame_tick_;
  state->int_mode = get_int_mode();
  state->pc = get_pc();
  state->sp = get_sp();
  state->hl = get_hl();
  state->de = get_de();
  state->bc = get_bc();
  state->is_halted = is_halted();
  state->ix = get_ix();
  state->iy = get_iy();
  state->alt_bc = get_alt_bc();
  state->alt_de = get_alt_de();
  state->alt_hl = get_alt_hl();
  state->alt_af = get_alt_af();
  state->a = get_a();
  state->f = get_f();
  state->i = get_i();
  state->r = get_r();
  state->iff1 = get_iff1();
  state->iff2 = get_iff2();
}

void MachineV2::restore_cpu_state(const V2CpuState* state) {
  frame_tick_ = state->tstates;
  set_int_mode(state->int_mode);
  set_pc(state->pc);
  set_sp(state->sp);
  set_hl(state->hl);
  set_de(state->de);
  set_bc(state->bc);
  set_is_halted(state->is_halted);
  set_ix(state->ix);
  set_iy(state->iy);
  set_alt_bc(state->alt_bc);
  set_alt_de(state->alt_de);
  set_alt_hl(state->alt_hl);
  set_alt_af(state->alt_af);
  set_a(state->a);
  set_f(state->f);
  set_i(state->i);
  set_r(state->r);
  set_iff1(state->iff1);
  set_iff2(state->iff2);
}

///////////////////////////////////////////
// Handlers
////////////////////////////////////////////

void MachineV2::on_tick(unsigned t) {
  base::on_tick(t);

  frame_tick_ += t;
  if (frame_tick_ >= kTicksPerFrame) {
    frame_tick_ %= kTicksPerFrame;
    events_ |= kEventInterrupt;
  }
}

void MachineV2::on_set_pc(z80::fast_u16 addr) {
  if (breakpoints_[addr]) events_ |= kEventBreakpoint;
  base::on_set_pc(addr);
}

z80::fast_u8 MachineV2::on_m1_fetch_cycle() {
  return base::on_m1_fetch_cycle();
}

bool MachineV2::on_handle_active_int() { return base::on_handle_active_int(); }

z80::fast_u8 MachineV2::on_input(z80::fast_u16 port) {
  auto res = input_state_.on_input(port);
  if (rzx_writer_) rzx_writer_->add_input(res);
  return res;
}