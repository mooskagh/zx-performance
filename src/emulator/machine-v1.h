#pragma once

#include <chips/chips/z80.h>
#include <glog/logging.h>

#include <limits>
#include <memory>

#include "emulator/input.h"
#include "emulator/memory.h"
#include "emulator/rzx-writer.h"

struct V1CpuState {
  uint64_t bc_de_hl_fa;
  uint64_t bc_de_hl_fa_;
  uint64_t wz_ix_iy_sp;
  uint64_t im_ir_pc_bits;
  uint64_t pins;
  uint32_t fetch_counter;
  int32_t frame_tick;

  void append_to_string(const Memory& memory, std::string* dst) const;
  size_t restore_from_string(std::string_view str, Memory* memory) const;
};

class MachineV1 {
 public:
  MachineV1();

  static constexpr uint8_t kEventInterrupt = 1;
  static constexpr uint8_t kEventBreakpoint = 2;

  Memory& memory() { return memory_; }
  const Memory& memory() const { return memory_; }
  RzxWriter* writer() const { return rzx_writer_.get(); }
  void load_snapshot(const std::string& filename);
  void save_snapshot(const std::string& filename,
                     libspectrum_id_t format = LIBSPECTRUM_ID_SNAPSHOT_SZX);
  uint32_t run(uint32_t num_ticks = std::numeric_limits<uint32_t>::max());
  InputState& input_state() { return input_state_; }
  const InputState& input_state() const { return input_state_; }
  void add_breakpoint(uint16_t addr) { breakpoints_[addr] = true; }
  void clear_breakpoints() { breakpoints_.fill(false); }
  void set_rzx_writer(std::unique_ptr<RzxWriter> rzx_writer);
  void save_cpu_state(V1CpuState* state) const;
  void restore_cpu_state(const V1CpuState* state);
  void set_event_mask(uint8_t mask) { event_mask_ = mask; }

  // Register accessor.
  uint16_t pc() const;

 private:
  Memory memory_;
  std::array<bool, 65536> breakpoints_;
  std::unique_ptr<RzxWriter> rzx_writer_;
  InputState input_state_;
  libspectrum_machine machine_model = LIBSPECTRUM_MACHINE_48;
  uint8_t event_mask_;

 public:
  int frame_tick_ = 0;

 private:
  z80_t z80_;

  static uint64_t tick_func(int num_ticks, uint64_t pins, void* user_data);
  static int trap_func(uint16_t pc, uint32_t ticks, uint64_t pins,
                       void* trap_user_data);
};