#include "emulator/rzx-writer.h"

#include <glog/logging.h>

#include "misc/filebuffer.h"

RzxWriter::RzxWriter() : rzx_(libspectrum_rzx_alloc()) {}

RzxWriter::~RzxWriter() { CHECK_EQ(libspectrum_rzx_free(rzx_), 0); }

void RzxWriter::add_snapshot(FuseSnap snap) {
  CHECK_EQ(libspectrum_rzx_add_snap(rzx_, snap.snap(), 0), 0);
  libspectrum_rzx_start_input(rzx_, libspectrum_snap_tstates(snap.snap()));
  snap.release();
}

void RzxWriter::add_interrupt(int ops) {
  VLOG(2) << "Interrupt, ops=" << ops;
  CHECK_EQ(
      libspectrum_rzx_store_frame(rzx_, ops, inputs_.size(), inputs_.data()),
      0);
  inputs_.clear();
}

void RzxWriter::add_input(uint8_t val) {
  VLOG(2) << "Input " << int(val);
  inputs_.push_back(val);
}

void RzxWriter::write(const std::string& filename) {
  libspectrum_byte* buffer = nullptr;
  size_t length = 0;

  FuseCreator creator;
  libspectrum_rzx_write(&buffer, &length, rzx_, LIBSPECTRUM_ID_SNAPSHOT_SZX,
                        creator.creator(), /* compress= */ 0, nullptr);
  write_string_to_file(
      filename, std::string_view(reinterpret_cast<char*>(buffer), length));
  libspectrum_free(buffer);
}
