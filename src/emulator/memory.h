#pragma once

#include <array>
#include <cstdint>
#include <string>

class FuseSnap;

class U8Accessor {
 public:
  constexpr U8Accessor(uint16_t addr) : addr_(addr) {}
  uint16_t addr() const { return addr_; }
  constexpr uint16_t size() const { return 1; }

 private:
  uint16_t addr_;
};

class U16Accessor {
 public:
  constexpr U16Accessor(uint16_t addr) : addr_(addr) {}
  uint16_t addr() const { return addr_; }
  constexpr uint16_t size() const { return 2; }

 private:
  uint16_t addr_;
};

class RangeAccessor {
 public:
  constexpr RangeAccessor(uint16_t addr, uint16_t size)
      : addr_(addr), size_(size) {}
  uint16_t addr() const { return addr_; }
  constexpr uint16_t size() const { return size_; }

 private:
  uint16_t addr_;
  uint16_t size_;
};

class Memory {
 public:
  void load_rom(const std::string& filename);

  uint8_t read(uint16_t addr) const { return memory_[addr]; }
  void write(uint16_t addr, uint8_t val) {
    if (addr >= rom_size_) memory_[addr] = val;
  }

  uint8_t& operator[](U8Accessor acc) { return memory_[acc.addr()]; }
  uint8_t operator[](U8Accessor acc) const { return memory_[acc.addr()]; }
  uint16_t& operator[](U16Accessor acc) {
    return *(reinterpret_cast<uint16_t*>(&memory_[acc.addr()]));
  }
  uint16_t operator[](U16Accessor acc) const {
    return *(reinterpret_cast<const uint16_t*>(&memory_[acc.addr()]));
  }
  std::string_view operator[](RangeAccessor acc) const {
    return {reinterpret_cast<const char*>(&memory_[acc.addr()]), acc.size()};
  }
  char* get_range_ptr(RangeAccessor acc) {
    return reinterpret_cast<char*>(&memory_[acc.addr()]);
  }
  const char* get_range_ptr(RangeAccessor acc) const {
    return reinterpret_cast<const char*>(&memory_[acc.addr()]);
  }

  void load_from_snap(const FuseSnap& snap);
  void save_to_snap(FuseSnap* snap);

 private:
  size_t rom_size_ = 0;
  std::array<std::uint8_t, 65536> memory_;
};
