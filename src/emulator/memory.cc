#include "emulator/memory.h"

#include <cstring>
#include <fstream>

#include "misc/fuselib.h"

namespace {
struct BankToOffset {
  int bank;
  uint16_t offset;
};

constexpr BankToOffset kBankToOffset[] = {
    {5, 0x4000}, {2, 0x8000}, {0, 0xc000}};
}  // namespace

void Memory::load_rom(const std::string& filename) {
  std::ifstream f(filename.c_str(), std::ios_base::binary);
  f.read(reinterpret_cast<char*>(&memory_[0]), memory_.size());
  rom_size_ = f.gcount();
}

void Memory::load_from_snap(const FuseSnap& snap) {
  for (const auto& bank : kBankToOffset) {
    std::memcpy(&memory_[bank.offset],
                CHECK_NOTNULL(libspectrum_snap_pages(snap.snap(), bank.bank)),
                0x4000);
  }
}

void Memory::save_to_snap(FuseSnap* snap) {
  for (const auto& bank : kBankToOffset) {
    if (libspectrum_snap_pages(snap->snap(), bank.bank) == nullptr) {
      libspectrum_snap_set_pages(snap->snap(), bank.bank,
                                 libspectrum_new(libspectrum_byte, 0x4000));
    }
    std::memcpy(CHECK_NOTNULL(libspectrum_snap_pages(snap->snap(), bank.bank)),
                &memory_[bank.offset], 0x4000);
  }
}