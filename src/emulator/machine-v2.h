#pragma once

#include <glog/logging.h>
#include <z80/z80.h>

#include <limits>
#include <memory>

#include "emulator/input.h"
#include "emulator/memory.h"
#include "emulator/rzx-writer.h"

struct V2CpuState {
  unsigned int_mode;
  uint32_t tstates;
  uint16_t pc;
  uint16_t sp;
  uint16_t hl;
  uint16_t de;
  uint16_t bc;
  uint16_t is_halted;
  uint16_t ix;
  uint16_t iy;
  uint16_t alt_bc;
  uint16_t alt_de;
  uint16_t alt_hl;
  uint16_t alt_af;
  uint8_t a;
  uint8_t f;
  uint8_t i;
  uint8_t r;
  bool iff1;
  bool iff2;

  void append_to_string(const Memory& memory, std::string* dst) const;
  size_t restore_from_string(std::string_view str, Memory* memory) const;
};

class MachineV2 : public z80::z80_cpu<MachineV2> {
 public:
  typedef z80::z80_cpu<MachineV2> base;
  static constexpr uint8_t kEventInterrupt = 1;
  static constexpr uint8_t kEventBreakpoint = 2;

  uint8_t run() { return run(kEventBreakpoint); }
  uint8_t run(uint8_t break_on_events,
              uint64_t max_ops = std::numeric_limits<uint64_t>::max());
  void run_frames(int how_many);
  Memory& memory() { return memory_; }
  const Memory& memory() const { return memory_; }
  void set_rzx_writer(std::unique_ptr<RzxWriter> rzx_writer);
  RzxWriter* writer() const { return rzx_writer_.get(); }
  void load_snapshot(const std::string& filename);
  void save_snapshot(const std::string& filename,
                     libspectrum_id_t format = LIBSPECTRUM_ID_SNAPSHOT_SZX);
  void save_cpu_state(V2CpuState* state) const;
  void restore_cpu_state(const V2CpuState* state);
  InputState& input_state() { return input_state_; }
  void add_breakpoint(uint16_t addr) { breakpoints_[addr] = true; }
  void set_event_mask(uint8_t) const {};

  // Handlers
  z80::fast_u8 on_read(z80::fast_u16 addr) { return memory_.read(addr); }
  void on_write(z80::fast_u16 addr, z80::fast_u8 n) { memory_.write(addr, n); }
  bool on_handle_active_int();
  z80::fast_u8 on_input(z80::fast_u16 port);
  void on_tick(unsigned t);
  void on_set_pc(z80::fast_u16 addr);
  z80::fast_u8 on_m1_fetch_cycle();

 private:
  Memory memory_;
  std::array<bool, 65536> breakpoints_;
  std::unique_ptr<RzxWriter> rzx_writer_;
  InputState input_state_;
  libspectrum_machine machine_model = LIBSPECTRUM_MACHINE_48;

  uint8_t events_ = 0;
  uint32_t frame_tick_ = 0;
};