#include "emulator/machine-v1.h"

#include <glog/logging.h>

#include <algorithm>

#include "misc/filebuffer.h"
#include "misc/fuselib.h"

namespace {
// constexpr int kTicksPerFrame = 224 * 312;
constexpr int kTicksPerFrame = 60000;

uint8_t get_high8(uint16_t v) { return v >> 8; }
uint8_t get_low8(uint16_t v) { return static_cast<uint8_t>(v); }
uint16_t make16(uint8_t hi, uint8_t lo) { return (hi << 8) | lo; }
}  // namespace

uint64_t MachineV1::tick_func(int num_ticks, uint64_t pins, void* user_data) {
  auto* self = reinterpret_cast<MachineV1*>(user_data);

  if ((self->frame_tick_ -= num_ticks) <= 0) {
    self->frame_tick_ += kTicksPerFrame;
    pins |= Z80_INT;
  }

  // Memory
  if (pins & Z80_MREQ) {
    const uint16_t addr = Z80_GET_ADDR(pins);
    if (pins & Z80_RD) {
      Z80_SET_DATA(pins, self->memory_.read(addr));
    } else if (pins & Z80_WR) {
      self->memory_.write(addr, Z80_GET_DATA(pins));
    }
  } else if ((pins & Z80_IORQ) && (pins & Z80_RD)) {
    // IO input
    auto data = self->input_state_.on_input(Z80_GET_ADDR(pins));
    if (self->rzx_writer_) self->rzx_writer_->add_input(data);
    Z80_SET_DATA(pins, data);
  }
  return pins;
}

int MachineV1::trap_func(uint16_t pc, uint32_t, uint64_t pins,
                         void* trap_user_data) {
  auto* self = reinterpret_cast<MachineV1*>(trap_user_data);
  if (0 != (pins & Z80_INT)) {
    if (self->rzx_writer_) {
      self->rzx_writer_->add_interrupt(self->z80_.fetch_counter);
    }
    self->z80_.fetch_counter = 0;
    if (0 != (self->event_mask_ & kEventInterrupt)) return kEventInterrupt;
  }
  if (0 != (self->event_mask_ & kEventBreakpoint) && self->breakpoints_[pc]) {
    return kEventBreakpoint;
  }
  return 0;
}

MachineV1::MachineV1() : frame_tick_(kTicksPerFrame) {
  z80_desc_t z80desc;
  z80desc.tick_cb = tick_func;
  z80desc.user_data = this;
  z80_init(&z80_, &z80desc);
  z80_trap_cb(&z80_, trap_func, this);
}

void MachineV1::save_cpu_state(V1CpuState* state) const {
  state->bc_de_hl_fa = z80_.bc_de_hl_fa;
  state->bc_de_hl_fa_ = z80_.bc_de_hl_fa_;
  state->wz_ix_iy_sp = z80_.wz_ix_iy_sp;
  state->im_ir_pc_bits = z80_.im_ir_pc_bits;
  state->pins = z80_.pins;
  state->fetch_counter = z80_.fetch_counter;
  state->frame_tick = frame_tick_;
}

void MachineV1::restore_cpu_state(const V1CpuState* state) {
  z80_.bc_de_hl_fa = state->bc_de_hl_fa;
  z80_.bc_de_hl_fa_ = state->bc_de_hl_fa_;
  z80_.wz_ix_iy_sp = state->wz_ix_iy_sp;
  z80_.im_ir_pc_bits = state->im_ir_pc_bits;
  z80_.pins = state->pins;
  z80_.fetch_counter = state->fetch_counter;
  frame_tick_ = state->frame_tick;
}

void MachineV1::load_snapshot(const std::string& filename) {
  FuseSnap snap;
  FileBuffer buffer(filename);
  CHECK_EQ(0, libspectrum_snap_read(
                  snap.snap(),
                  reinterpret_cast<const std::uint8_t*>(buffer.data().data()),
                  buffer.data().size(), LIBSPECTRUM_ID_UNKNOWN,
                  buffer.filename().c_str()));

  memory().load_from_snap(snap);

  frame_tick_ = std::clamp(
      static_cast<int>(kTicksPerFrame - libspectrum_snap_tstates(snap.snap())),
      1, kTicksPerFrame);
  z80_set_pc(&z80_, libspectrum_snap_pc(snap.snap()));
  z80_set_a(&z80_, libspectrum_snap_a(snap.snap()));
  z80_set_f(&z80_, libspectrum_snap_f(snap.snap()));
  z80_set_sp(&z80_, libspectrum_snap_sp(snap.snap()));
  z80_set_hl(&z80_, libspectrum_snap_hl(snap.snap()));
  z80_set_de(&z80_, libspectrum_snap_de(snap.snap()));
  z80_set_bc(&z80_, libspectrum_snap_bc(snap.snap()));
  z80_set_ix(&z80_, libspectrum_snap_ix(snap.snap()));
  z80_set_iy(&z80_, libspectrum_snap_iy(snap.snap()));
  z80_set_i(&z80_, libspectrum_snap_i(snap.snap()));
  z80_set_r(&z80_, libspectrum_snap_r(snap.snap()));
  z80_set_bc_(&z80_, libspectrum_snap_bc_(snap.snap()));
  z80_set_de_(&z80_, libspectrum_snap_de_(snap.snap()));
  z80_set_hl_(&z80_, libspectrum_snap_hl_(snap.snap()));
  z80_set_af_(&z80_, make16(libspectrum_snap_a_(snap.snap()),
                            libspectrum_snap_f_(snap.snap())));
  z80_set_iff1(&z80_, libspectrum_snap_iff1(snap.snap()));
  z80_set_iff2(&z80_, libspectrum_snap_iff2(snap.snap()));
  z80_set_im(&z80_, libspectrum_snap_im(snap.snap()));
  z80_set_is_halted(&z80_, libspectrum_snap_halted(snap.snap()));
  z80_set_ei_pending(&z80_, false);
  if (rzx_writer_) rzx_writer_->add_snapshot(std::move(snap));
}

void MachineV1::save_snapshot(const std::string& filename,
                              libspectrum_id_t format) {
  FuseSnap snap;
  memory().save_to_snap(&snap);

  libspectrum_snap_set_machine(snap.snap(), machine_model);
  libspectrum_snap_set_tstates(
      snap.snap(),
      std::clamp(kTicksPerFrame - frame_tick_, 0, kTicksPerFrame - 1));
  libspectrum_snap_set_pc(snap.snap(), z80_pc(&z80_));
  libspectrum_snap_set_a(snap.snap(), z80_a(&z80_));
  libspectrum_snap_set_f(snap.snap(), z80_f(&z80_));
  libspectrum_snap_set_sp(snap.snap(), z80_sp(&z80_));
  libspectrum_snap_set_hl(snap.snap(), z80_hl(&z80_));
  libspectrum_snap_set_de(snap.snap(), z80_de(&z80_));
  libspectrum_snap_set_bc(snap.snap(), z80_bc(&z80_));
  libspectrum_snap_set_halted(snap.snap(), z80_is_halted(&z80_));
  libspectrum_snap_set_ix(snap.snap(), z80_ix(&z80_));
  libspectrum_snap_set_iy(snap.snap(), z80_iy(&z80_));
  libspectrum_snap_set_i(snap.snap(), z80_i(&z80_));
  libspectrum_snap_set_r(snap.snap(), z80_r(&z80_));
  libspectrum_snap_set_bc_(snap.snap(), z80_bc_(&z80_));
  libspectrum_snap_set_de_(snap.snap(), z80_de_(&z80_));
  libspectrum_snap_set_hl_(snap.snap(), z80_hl_(&z80_));
  libspectrum_snap_set_iff1(snap.snap(), z80_iff1(&z80_));
  libspectrum_snap_set_iff2(snap.snap(), z80_iff2(&z80_));
  libspectrum_snap_set_im(snap.snap(), z80_im(&z80_));
  libspectrum_snap_set_a_(snap.snap(), get_high8(z80_af_(&z80_)));
  libspectrum_snap_set_f_(snap.snap(), get_low8(z80_af_(&z80_)));

  unsigned char* buffer = nullptr;
  size_t length = 0;
  int flags;
  FuseCreator creator;
  CHECK_EQ(libspectrum_snap_write(&buffer, &length, &flags, snap.snap(), format,
                                  creator.creator(), 0),
           0);

  if (flags & LIBSPECTRUM_FLAG_SNAPSHOT_MAJOR_INFO_LOSS) {
    LOG(WARNING) << "A large amount of information has been lost in conversion;"
                    " the snapshot probably won't work";
  } else if (flags & LIBSPECTRUM_FLAG_SNAPSHOT_MINOR_INFO_LOSS) {
    LOG(WARNING) << "Some information has been lost in conversion;"
                    " the snapshot may not work";
  }

  write_string_to_file(
      filename, std::string_view(reinterpret_cast<char*>(buffer), length));
  libspectrum_free(buffer);
}

uint32_t MachineV1::run(uint32_t num_ticks) {
  return z80_exec(&z80_, num_ticks);
}

uint16_t MachineV1::pc() const { return z80_pc(const_cast<z80_t*>(&z80_)); }

void MachineV1::set_rzx_writer(std::unique_ptr<RzxWriter> rzx_writer) {
  rzx_writer_ = std::move(rzx_writer);
}
