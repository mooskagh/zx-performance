#pragma once

#include "ui/view.h"

class ProgressBar : public UiView {
 public:
  void set_val(float lo, float hi, float max);

 private:
  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  float lo_ = 0.0f;
  float hi_ = 0.0f;
  float max_ = 1.0f;
};