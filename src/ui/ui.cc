#include "ui/ui.h"

Ui::Ui()
    //: window_(sf::VideoMode(1280, 720), "ZX Spectrum speedrun strat finder")
    //{}
    : window_(sf::VideoMode(788, 601), "ZX Spectrum speedrun strat finder") {
  progress_bar_.move(10.f, 5.f);
  progress_bar_.scale(768.f, 5.f);
  window_.setActive(false);
}

bool Ui::process_events() {
  sf::Event event;
  while (window_.pollEvent(event)) {
    if (event.type == sf::Event::Closed) window_.close();
  }
  return window_.isOpen();
}

void Ui::draw() {
  window_.setActive(true);
  window_.clear(sf::Color::Black);
  window_.draw(progress_bar_, progress_bar_.getTransform());
  if (zx_screen_) window_.draw(*zx_screen_, zx_screen_->getTransform());
  window_.display();
  window_.setActive(false);
}

void Ui::set_zx_screen_memory(const Memory& memory) {
  zx_screen_ = std::make_unique<ZxScreen>(memory);
  zx_screen_->move(10.f, 15.f);
  zx_screen_->scale(3.0f, 3.0f);
}

void Ui::remove_zx_screen() { zx_screen_.reset(); }