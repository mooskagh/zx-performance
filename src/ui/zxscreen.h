#pragma once

#include "emulator/memory.h"
#include "ui/view.h"

class ZxScreen : public UiView {
 public:
  ZxScreen(const Memory& memory);

 private:
  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  sf::Image build_image() const;

  const Memory& memory_;
};