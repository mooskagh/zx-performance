#pragma once

#include <absl/flags/flag.h>

#include <filesystem>
#include <string>

inline std::string get_data_dir() { return "../../data/"; }

inline std::string get_time_string() {
  time_t rawtime;
  struct tm* timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, sizeof(buffer), "%Y%m%d-%H%M", timeinfo);
  return buffer;
}

inline std::string get_date_string() {
  time_t rawtime;
  struct tm* timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, sizeof(buffer), "%Y%m%d", timeinfo);
  return buffer;
}

inline std::string get_filename(const std::string& game, const std::string& dir,
                                const std::string& run,
                                const std::string& suffix = "",
                                bool include_time = false,
                                const std::string& extension = "",
                                bool date_dir = false) {
  std::string res = get_data_dir() + game + "/" + dir;
  if (date_dir) res += "/" + get_date_string();
  std::filesystem::create_directories(res);
  res += "/" + run;
  if (include_time) res += "_" + get_time_string();
  if (!suffix.empty()) res += "_" + suffix;
  res += "." + (extension.empty() ? dir : extension);
  return res;
}