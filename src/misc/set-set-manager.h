#pragma once

#include <absl/container/flat_hash_map.h>

#include <cstdint>
#include <utility>
#include <vector>

struct Set16 {
  Set16() = default;
  explicit Set16(uint16_t set) : set(set) {}
  uint16_t set = 0;

  bool is_superset_of(const Set16& other) const {
    return (set & other.set) == other.set;
  }

  uint8_t count() const { return __builtin_popcount(set); }
};

struct SetSetId {
  explicit SetSetId(uint16_t idx) : idx(idx) {}
  SetSetId(const SetSetId&) = default;
  SetSetId() : idx(0) {}
  void reset() { idx = 0; }
  uint16_t idx;
};

class SetSetManager {
 public:
  SetSetManager();
  // Returns <is_duplicate, new set id>
  std::pair<bool, SetSetId> update(SetSetId set_set, Set16 set16);

 private:
  std::vector<std::vector<Set16>> set_sets_;
  absl::flat_hash_map<uint64_t, SetSetId> hash_to_set_id_;
};