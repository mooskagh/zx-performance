#pragma once

#include <vector>

class DisjointSets {
 public:
  DisjointSets(size_t size) {
    while (leaders_.size() < size) append();
  }

  void append() { leaders_.push_back(leaders_.size()); }
  size_t get_leader(size_t element) const {
    size_t res = element;
    while (leaders_[res] != res) res = leaders_[res];
    while (leaders_[element] != res) {
      auto tmp = leaders_[element];
      leaders_[element] = res;
      element = tmp;
    }
    return res;
  }
  void unite(size_t x, size_t y) { leaders_[y] = x; }
  bool same(size_t x, size_t y) const { return get_leader(x) == get_leader(y); }

 private:
  mutable std::vector<size_t> leaders_;
};