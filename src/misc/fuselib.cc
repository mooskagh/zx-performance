#include "misc/fuselib.h"

namespace {

class Initializer {
 public:
  Initializer() { CHECK_EQ(0, libspectrum_init()); }
};
static Initializer initializer;

}  // namespace