#pragma once
#include <glog/logging.h>
#include <libspectrum.h>

class FuseSnap {
 public:
  FuseSnap() : snap_(libspectrum_snap_alloc()) {}
  FuseSnap(FuseSnap&& other) {
    snap_ = other.snap_;
    other.snap_ = nullptr;
  }
  ~FuseSnap() {
    if (snap_) libspectrum_snap_free(snap_);
  }
  libspectrum_snap* snap() const { return snap_; }
  void release() { snap_ = nullptr; }

 private:
  libspectrum_snap* snap_;
};

class FuseCreator {
 public:
  FuseCreator() : creator_(libspectrum_creator_alloc()) {
    CHECK_EQ(libspectrum_creator_set_program(creator_, "ZX Stratfinder"), 0);
    CHECK_EQ(libspectrum_creator_set_minor(creator_, 0), 0);
    CHECK_EQ(libspectrum_creator_set_major(creator_, 1), 0);
  }
  ~FuseCreator() { libspectrum_creator_free(creator_); }

  libspectrum_creator* creator() const { return creator_; }

 private:
  libspectrum_creator* const creator_;
};