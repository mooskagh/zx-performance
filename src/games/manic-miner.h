#include "emulator/memory.h"

namespace manic_miner {

static constexpr U8Accessor level_num{0x8407};

static constexpr RangeAccessor cavern_attrs{0x5e00, 0x200};
static constexpr RangeAccessor cavern_pixels{0x7000, 0x1000};
static constexpr U8Accessor willy_y{0x8068};
static constexpr U8Accessor willy_animation{0x8069};
static constexpr U8Accessor willy_movement{0x806a};
static constexpr U8Accessor willy_fall{0x806b};
static constexpr U16Accessor willy_location{0x806c};
static constexpr U8Accessor willy_jumping{0x806e};
static constexpr U8Accessor last_item_drawn_attr{0x8074};
static constexpr U8Accessor item1_attr{0x8075 + 0};
static constexpr U8Accessor item2_attr{0x8075 + 5};
static constexpr U8Accessor item3_attr{0x8075 + 10};
static constexpr U8Accessor item4_attr{0x8075 + 15};
static constexpr U8Accessor item5_attr{0x8075 + 20};
static constexpr U8Accessor remaining_air{0x80bc};
static constexpr U8Accessor game_clock{0x80bd};
static constexpr U8Accessor eugene_y{0x80dc};
static constexpr U8Accessor kong_status{0x80DB};  // 0 = on the ledge
static constexpr RangeAccessor guardians{0x80be, 29 + 1 + 1 + 35};
static constexpr U8Accessor portal_attr{0x808f};
static constexpr U8Accessor screen_flash{0x8458};
static constexpr RangeAccessor score{0x841F, 16};
static constexpr RangeAccessor all_mem{0x4000, 0xc000};
static constexpr U8Accessor switch_src{0x7506};
static constexpr U8Accessor switch_dst{0x8065};

}  // namespace manic_miner
