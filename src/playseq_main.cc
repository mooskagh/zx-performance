#include <glog/logging.h>

#include <cstdio>
#include <stdexcept>

#include "emulator/input.h"
#include "emulator/machine-v1.h"
#include "emulator/machine-v2.h"
#include "games/manic-miner.h"
#include "misc/config.h"
#include "ui/ui.h"
#include "ui/zxscreen.h"

template <class T>
void Run(bool use_ui) {
  std::unique_ptr<Ui> ui;
  if (use_ui) ui = std::make_unique<Ui>();

  T machine;

  if (ui) ui->set_zx_screen_memory(machine.memory());

  machine.memory().load_rom(get_data_dir() + "roms/48.rom");

  machine.set_event_mask(T::kEventBreakpoint);
  machine.memory().load_rom(get_data_dir() + "roms/48.rom");
  machine.load_snapshot(get_filename("manic-miner", "szx", "lvl1"));
  machine.add_breakpoint(0x870e);  // Frame.
  machine.add_breakpoint(0x8908);  // Death.

  for (const auto& input : get_inputs_from_file(get_data_dir() + "moves.txt")) {
    machine.input_state() = input;
    machine.run();
    if (ui) ui->draw();
  }

  machine.input_state() = {};
  machine.set_event_mask(T::kEventInterrupt);
}

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;

  if (argc < 2) throw std::runtime_error("blah2");

  if (argv[1] == std::string("A")) {
    Run<MachineV1>(argc > 2);
  } else if (argv[1] == std::string("B")) {
    Run<MachineV2>(argc > 2);
  } else {
    throw std::runtime_error("blah");
  }
}